# HeXXen 1733 Foundry VTT System

HeXXen 1733 game system for Foundry VTT (German)

Provides character sheets for Jäger (PC) and NPC, basic feature implementation and an adapted roll tool.

Unfortunately, for legal reasons I can no longer provide items based on "Regel-Wiki" content directly with the game system. But there will be official modules available soon filling that gap.

More details on how to use the system can be found in our german [Wiki](https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/wikis/home).

## Installation

We moved from GitHub to GitLab, so if you have bookmarks pointing to the old location, you should update them now.
  
It is strongly recommended that you install from the **release** or **beta** branch! (On GitHub these were known as master (now release) and stable (now beta).)

It not recommended to install from branch **develop**, as it is subject of ongoing development and can break at any time. New features will be available in **beta** as soon as they seem usable, so there is no need to install from **develop** branch. (If you install from **develop** anyways, be aware that Foundry can not identify changes as long as the version number does not change. You might have to re-install the system in order to update it.)

Install link for **release** version: https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/raw/release/system.json

Install link for **beta** version: https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/raw/beta/system.json

Install link for **Foundry 0.7.x** version: https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/raw/release-fvtt-0.7.x/system.json   
Use this version if you want to stay with `Foundry VTT 0.7.x`. It won't automatically update to versions not compatible with Foundry VTT 0.7.x. It might get bugfixes as needed, but it will not get any new features.

You can also refer to our german Wiki pages [Installation](https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/wikis/de/Installation) and [Release Notes](https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/wikis/de/Release-Notes).

If you have old chat messages created by the addon module **Special Dice Roller** it is recommended to still install and activate the module, otherwise the dice images will be missing.   
Install link for the **Special Dice Roller** module: https://github.com/BernhardPosselt/foundryvtt-special-dice-roller/raw/master/module.json
(Don't forget to activate the module in your game world.)

## Legal Notice

Dieses Produkt wurde unter Lizenz erstellt. HeXXen 1733 und sein Logo sind eingetragene Marken von Ulisses Spiele Deutschland, den U.S.A. und anderen Ländern. Ulisses Spiele und sein Logo sind eingetragene Marken der Ulisses Medien und Spiele Distribution GmbH.

Dieses Werk enthält Material, das durch Ulisses Spiele und/oder andere Autoren urheberrechtlich geschützt ist. Solches Material wird mit Erlaubnis im Rahmen der Vereinbarung über Gemeinschaftsinhalte für HeXXen-1733-Scriptorium verwendet.

Alle anderen Originalmaterialien in diesem Werk sind Copyright 2020 von Martin Brunninger und werden im Rahmen der Vereinbarung über Gemeinschaftsinhalte für HeXXen-1733-Scriptorium veröffentlicht.

https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Skriptorium
