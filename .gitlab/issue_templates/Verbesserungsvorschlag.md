**Womit arbeitest du?**
 - OS: [z.B. Windows, MacOS, Linux, NodeJS] [Version]
 - Browser: [z.B. Electron, Chrome, Safari, Firefox, Edge] [Version]
 - Foundry-Version: [z.B. 10.288]
 - HeXXen-Version: [z.B. 1.1.0]


**Erläutere bitte was verbessert werden könnte. Ggf. erläutere auch, warum dies eine Verbesserung ist.**


**Wie könnte die Verbesserung aussehen? Falls sinnvoll inkl. Screenshot oder Skizze.**
